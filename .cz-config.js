'use strict';
module.exports = {
    types: [{
        value: 'post',
        name: '🔥 新文章'
    }, {
        value: 'config',
        name: '🛠️ Yaml or theme config'
    }, {
        value: 'docs',
        name: '📚 說明文件 (md)'
    }
    ],
    messages: {
        type: '選擇分類',
        subject: '填寫這次commit修改了什麼，未來看這段commit msg 自己必須要能理解為原則',
    },
    typePrefix: '[',
    typeSuffix: ']',
    upperCaseSubject: true,
    allowCustomScopes: true,
    skipQuestions: ['body', 'scope', 'breaking', 'footer', 'confirmCommit'],
};