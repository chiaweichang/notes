---
title: Recommended Tools
date: 2021-10-27 22:20:36
categories:
tags: python
---

## Python

### Rich - Python library for rich text and beautiful formatting in the terminal.

* https://github.com/willmcgugan/rich/blob/master/README.cn.md
* {% youtube tIHENsYg7Fs %}

## Browser

### Brave
* https://brave.com/

## Terminal

### 配置一个漂亮的Windows PowerShell
* https://www.youtube.com/watch?v=YCxTjW48qDM
* https://www.youtube.com/watch?v=LolXx897FEM
* https://www.youtube.com/watch?v=ccg9oEBZFBk

#### Windows Terminal

* https://github.com/microsoft/terminal/releases/tag/v1.12.2931.0
* https://github.com/microsoft/terminal

#### PowerShell

* https://github.com/PowerShell/powershell/releases

#### oh-my-posh

* https://ohmyposh.dev/docs/themes
