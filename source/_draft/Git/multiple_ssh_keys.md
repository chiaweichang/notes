---
title: Multiple SSH keys for different accounts
date: 2021-10-24 01:26:25
categories: git
tags: git
---


The Note is refering to this [Tutorial](https://coderwall.com/p/7smjkq/multiple-ssh-keys-for-different-accounts-on-github-or-gitlab)


# Scenario
I have two accounts, one for work, and the other for personal use.

* 💼 work = `git@gitlab.com:company_name/note.git`
* 🏠 personal = `git@gitlab.com:chiaweichang/note.git`

# Create a home ssh key

``` bash
$ ssh-keygen -t rsa -C "your_name@home_email.com"
Generating public/private rsa key pair.
Enter file in which to save the key (/home/user_name/.ssh/id_rsa):
```

input `id_rsa_home` on the screen.

# Do the same thing with your work email.

``` bash
$ ssh-keygen -t rsa -C "your_name@company_email.com"
```

input `id_rsa_company` on the screen.

# And we will see that below.

``` bash
$ ls ~/.ssh

id_rsa_home  id_rsa_company  id_rsa_home.pub  id_rsa_company.pub
```

# Add key name into `config` file

```
$ vim ~/.ssh/config
```

```
# Home account
Host home.github.com
  HostName github.com
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/id_rsa_home

# Company account
Host company.github.com
  HostName github.com
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/id_rsa_company
```

# restart it

``` bash
$ eval ssh-agent -s
$ ssh-add -D
$ ssh-add -l
2048 d4:e0:39:e1:bf:6f:e3:26:14:6b:26:73:4e:b4:53:83 /home/user/.ssh/id_rsa_home (RSA)
2048 7a:32:06:3f:3d:6c:f4:a1:d4:65:13:64:a4:ed:1d:63 /home/XXXX/.ssh/id_rsa_company (RSA)
```

# if nothing is shown on the screen, you can add it manually.

``` bash
$ ssh-add ~/.ssh/id_rsa_company
$ ssh-add ~/.ssh/id_rsa_home
```

# Test if they worked?

``` bash
$ ssh -T git@home.github.com
Hi home_user! You've successfully authenticated, but GitHub does not provide shell access.

$ ssh -T git@company.github.com
Hi company_user! You've successfully authenticated, but GitHub does not provide shell access.
```

# You might need to add `git config` without `--global` for different repositories.

``` bash
$ cd ~/home_project
$ git config user.name "home_user"
$ git config user.email "your_name@home_email.com"
```
