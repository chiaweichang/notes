---
title: Git Scenario
date: 2021-10-26 00:54:00
categories: git
tags: git
---

# 🌵 Most Used
## When starting without repo in your computer

you need to clone repo from gitlab

``` bash
$ git clone --recursive git@gitlab.com:USERNAME/GIT_REPO_NAME.git

Cloning into 'GIT_REPO_NAME'...
remote: Enumerating objects: 271, done.
remote: Counting objects: 100% (271/271), done.
remote: Compressing objects: 100% (144/144), done.
remote: Total 7081 (delta 140), reused 233 (delta 116), pack-reused 6810
Receiving objects: 100% (7081/7081), 1.90 MiB | 1.99 MiB/s, done.
Resolving deltas: 100% (4618/4618), done.
```

## How to check repo status

you need to check if there is new code status on your git dir.

``` bash
$ git status
On branch master
Your branch is up to date with 'origin/master'.

nothing to commit, working tree clean
```

## Forgot to add one file after committing code
```bash=
git add file1
git commit --amend --no-edit
```

# 🌵 branch
## When starting a feature without any branch on the Gitlab

You should create a new branch and checkout to new branch

``` bash
$ git checkout -b new_branch_name
$ git br
  master

* new_branch_name
```

## When starting a feature in existing branch on the GitLab

Just checkout to new branch

``` bash
$ git branch -a

* master

  remotes/origin/HEAD -> origin/master
  remotes/origin/new_branch_name

$ git checkout origin/new_branch_name -b new_branch_name

Branch 'new_branch_name' set up to track remote branch 'new_branch_name' from 'origin'.
Switched to a new branch 'new_branch_name'

$ git branch
  master

* new_branch_name

```

## When you want to commit code on your `dev` branch

* Don't use `git add *` `git add --all`
  + Why? You MAY ADD some file you don't know, I don't think it's good idea to add these file to gitlab.
* Don't commit on `master` branch
  + Why? Because only `maintainer` have permission to add commit into `master` branch
* Please use `commitizen` tools to write your commit messages
  + Make commit msg clear is necessary, that would help other member to know the commit easily.

``` bash
$ git add file1 file2 file3

$ git status
On branch new_branch_name
Your branch is up to date with 'origin/new_branch_name'.

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        new file:   file1
        new file:   file2
        new file:   file3

$ git cz
```


## When delete branch that is already merge in master

``` bash
$ git branch -d new_branch_name

Deleted branch new_branch_name (was e14a5e6).
```


# 🌵 Pull & Push
## When pull new commit on Gitlab

  + make sure you have commited all your code
  + or stash all your uncommit code by cmd `git stash --include-untracked`

```bash
# Get new code from master
$ git pull origin master
```

or

``` bash
# Get new code from the specific branch
$ git pull origin new_branch_name

From gitlab.com:moxa/ibg/test/products/autotest/GIT_REPO_NAME
 * branch            new_branch_name -> FETCH_HEAD
Already up to date.
```

## When push new commit on Gitlab
YOU CAN NOT PUSH ANY COMMIT INTO `MASTER` IF YOU ARE NOT `MAINTAINER` !!

`git push <remote-name> <local-branch-name>:<remote-branch-name>`

``` bash
$ git push origin new_branch_name

Everything up-to-date
```


# 🌵 Merge request
## Before you send Merge request on Gitlab?

![](/Images/gitflow.jpg)

* Make sure you test your before and after pulling from `master`
* First, git clone repo from gitlab.com
* Make new branch if you want to dev new a feature. Please don't commit code on `master` branch!
* Before sending a request to `maintainer`
    - Test your code on your `dev` branch and make sure it can work on your test and that won't make other's code mess.
    - When no error occurs on your commit, then `git pull origin master`
    - Make sure your pipeline works well then you can send `Merge Request`.
    - If anythings went wrong, please contact your `maintainer`.


# 🌵 Rebase OR Merge
https://github.com/gitforteams/diagrams/blob/master/flowcharts/rebase-or-merge.png

# Reference

[使用moxa git flow, 以提高測試準確性及品質(避免開發中的測項，被包進daily test)](https://gitlab.com/moxa/ibg/test/products/autotest/GIT_REPO_NAME/-/issues/7)
