---
title: Git config settings
date: 2021-10-24 01:19:25
categories: git
tags: git
---

# Add PC public SSH key into Github/Gitlab

``` bash
ssh-keygen
cat ~/.ssh/id_rsa.pub
```

# Add global settings

``` bash
git config --global user.name "ChiaWeiChang"
git config --global user.email chiaweichang@ntu.edu.tw
git config --global core.editor vim
```

# Useful Alias
[git.aliases](https://github.com/Bash-it/bash-it/blob/master/aliases/available/git.aliases.bash)

``` bash
git config --global alias.co checkout
git config --global alias.br branch
git config --global alias.ci commit
git config --global alias.st status
git config --global alias.last 'log -1 HEAD'
git config --global alias.sm submodule
git config --global alias.gg 'log --oneline --abbrev-commit --all --graph --decorate --color'
```

# 中文亂碼處理
[miniGW 中文亂碼](https://gist.github.com/nightire/5069597)

``` bash
git config --global core.quotepath false
git config --global gui.encoding utf-8
git config --global i18n.commit.encoding utf-8
git config --global i18n.logoutputencoding utf-8
export LESSCHARSET=utf-8
```
