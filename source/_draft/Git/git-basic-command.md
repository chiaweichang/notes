---
title: git basic command
date: 2021-10-26 00:53:00
categories: git
tags: git
---

# Basic
``` bash
git clone https://gitlab.com/XXXXXX.git     # 下載
git init                                    # 將該資料夾設定為git專案
git status                                  # 確認目前狀態
git diff                                    # 查看差異
```

## `git add` 新增檔案
``` bash
-i, --interactive     interactive picking
-p, --patch           select hunks interactively
-u, --update          update tracked files
-A, --all             add changes from all tracked and untracked files
```

## `git commit` 提交
``` bash
-m, --message <message>             commit message
--amend                             amend previous commit
-u, --untracked-files[=<mode>]      show untracked files, optional modes: all, normal, no. (Default: all)
```

## `git pull` 拉取commit
``` bash
--recurse-submodules[=<on-demand>]         control for recursive fetching of submodules
--ff                  allow fast-forward
--ff-only             abort if fast-forward is not possible
-f, --force           force overwrite of local branch
-t, --tags            fetch all tags and associated objects
```

``` bash
git pull
git pull --rebase
git push origin master
```

## `git push` 推送commit
```
--tags                push tags (can't be used with --all or --mirror)
-f, --force           force updates
```

``` bash
# -u|--set-upstream
git push -u <remote name> <branch name>
git push --set-upstream <remote name> <branch name>
```

* [git-pull-rebase-preserve-merges](https://stackoverflow.com/questions/21364636/git-pull-rebase-preserve-merges)

```
git pull --rebase=preserve
```



## `git stash` 暫存
``` bash
git stash save
git stash save "msg"

git stash -u
git stash --include-untracked
git stash save --include-untracked

git stash list

git stash apply
git stash apply stash@{0}
git stash pop
git stash pop stash@{0}
git stash drop
git stash drop stash@{0}

git stash show stash@{0}
```

## `git branch`
``` bash
git branch
git branch --all
git branch --list
git branch --remote

# delete
git branch --delete
git branch -d BRANCH_YOU_WANT_TO_DELETE

# Add new branch from specified commit
git checkout -b bird 657fce7

# rename a branch while pointed to any branch
git branch -m <oldname> <newname>
git branch --move <oldname> <newname>

# rename the current branch

git branch -m <newname>
git branch --move <newname>
```

## `git blame`
``` bash
git blame -L 5,10 index.html
```

## `git reset`

``` bash
git reset --patch

# Soft way
git reset HEAD^

# Hard way, clear everything
git reset --hard HEAD^
git push -f

git reset ORIG_HEAD --hard
```

## `git subModule`
### clone with submodule
``` bash
git clone --recurse-submodules EXAMPLE_URL
```
### Add submodule
``` bash
git submodule add SUBMODULE_URL
git submodule init
git submodule update --init --recursive
```
### To update submodules, we can use
``` bash
git submodule update --recursive --remote
# or
git pull --recurse-submodules
```
### Remove Submodule
* Reference from https://forum.freecodecamp.org/t/how-to-remove-a-submodule-in-git/13228
* Delete the section referring to the submodule from the .gitmodules file
* Stage the changes via git add .gitmodules
* Delete the relevant section of the submodule from .git/config.
* Run git rm --cached path_to_submodule (no trailing slash)
* Run rm -rf .git/modules/path_to_submodule
* Commit the changes with ```git commit -m "Removed submodule "
* Delete the now untracked submodule files rm -rf path_to_submodule

## `git log`
``` bash
git log
git log --oneline
git log --oneline --graph
git log HEAD^
git log HEAD~3
git log working..broken
git log -S foo

git show {commit_id}
```

## `git tag`
``` bash
git tag {tag_name} {commit_id}      # add tag on commit
git tag                             # view tag
git tag -d v1.4-lw                  # delete

# -a|--annotate add msg into tag
git tag -a v1.4 -m "my version 1.4"

# Push tag
git push --tags
git push origin <tagname>
git push origin --delete <tagname>
```

## `.gitignore`
* [github gitignore](https://github.com/github/gitignore)

## `git checkout`

* [Git-Branching-Remote-Branches](https://git-scm.com/book/en/v2/Git-Branching-Remote-Branches)

### checkout to new local branch
``` bash
git checkout master
git branch new_branch
git checkout new_branch
``
or simple way
``` bash
git checkout -b new_branch master
```
### checkout to remote branch
``` bash
git checkout -b local_branch_name origin/remote_branch_name
# or
git checkout --track origin/remote_branch_name
```

## `git remote`
``` bash
git remote -v
git remote --verbose

git remote add origin {origin_git_repo_url}
git remote add upstream ssh://git@gitlab.com
git pull upstream

git remote rm upstream
git remote remove upstream
git push --all upstream

git remote rename origin official
```

## `git merge`
``` bash
git merge <branch name>
git merge --no-ff <branch name>
git merge -X ours origin/master
git merge -X theirs origin/master
git merge origin/master
git merge –abort
git merge --strategy-option=their incoming_branch

# tools for solving conflict
git-imerge
```

## `git rebase`
``` bash
git rebase origin master
git rebase --interactive|-i
```

## `git clean`
``` bash
# Just exercise, won't delete anything
git clean -n

# Clean all untracked file. exclude file in .gitignore
git clean -f　　
git clean -f <path>

# recurse into untracked directories
git clean -df

# Clean all untracked file. Include file in .gitignore will be deleted
git clean -xf
```

## Other

``` bash
git reflog
git cherry-pick -x
git fetch
git revert
git bisect
```

# Reference

* [Git Notes - HackMD](https://hackmd.io/@leoluyi/Skw4bkzTe)
* [Git: 四種將分支與主線同步的方法 | Summer。 桑莫。 夏天](https://cythilya.github.io/2018/06/19/git-merge-branch-into-master/)
* [Git團隊手冊 - HackMD](https://hackmd.io/oQWnH55ESUuTkGL6-97cAA)
* [Git 版本控制系統(2) 開 branch 分支和操作遠端 repo. – ihower { blogging}](https://ihower.tw/blog/archives/2620)
* [gitforteams/diagrams: Source files for diagrams used in Git for Teams workshops, sessions, videos, and the book.](https://github.com/gitforteams/diagrams)
* [在 Merge 之前想試試看有沒有衝突？ - Git 短文 | 高見龍](https://gitbook.tw/posts/2018-07-20-git-merge-dry-run)
* dry-run git