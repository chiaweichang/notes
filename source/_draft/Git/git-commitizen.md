---
title: Git PlugIn - Commitizen
date: 2021-10-26 00:51:00
categories: git
tags: git
---

# Commitizen
## How to install Commitizen

* [Download](http://commitizen.github.io/cz-cli/)
* [In windows](https://stackoverflow.com/questions/27864040/fixing-npm-path-in-windows-8-and-10)
* [VScode Marketplace](https://marketplace.visualstudio.com/items?itemName=KnisterPeter.vscode-commitizen)
* [Conventional commit messages as a global utility](https://github.com/commitizen/cz-cli#conventional-commit-messages-as-a-global-utility)
* Linux
```
sudo npm install -g commitizen cz-conventional-changelog
echo '{ "path": "cz-conventional-changelog" }' > ~/.czrc
```

## Commitizen Config (.cz-config.js)
| item     | description                      |
| -------- | -------------------------------- |
| feat     | ✨ 新增功能                       |
| test     | 👀 新增測試                       |
| fix      | 🐞 修復臭蟲                       |
| refactor | 🛠️ 重構程式                       |
| style    | 💅 修改格式 (pylint, pycodestyle) |
| docs     | 📚 說明文件 (md)                  |
| ci       | ⏰ 軟體工程 (yaml, config)        |
| chore    | 🗯️ 其他項目                       |


You can put `.cz-config.js` in your repo to make sure everyone's commit format is aligned.

``` js
'use strict';
module.exports = {
    types: [{
            value: 'feat',
            name: '✨ 新增功能'},{
            value: 'test',
            name: '👀 新增測試'},{
            value: 'fix',
            name: '🐞 修復臭蟲'},{
            value: 'refactor',
            name: '🛠️ 重構程式'},{
            value: 'style',
            name: '💅 修改格式 (pylint, pycodestyle)'},{
            value: 'docs',
            name: '📚 說明文件 (md)'},{
            value: 'ci',
            name: '⏰ 軟體工程 (yaml, config)'},{
            value: 'chore',
            name: '🗯️ 其他項目'
        }
    ],
    messages: {
        type: '選擇分類',
        scope: '影響範圍，填寫 module or testcase scope. e.g. platformcmd, RESTful',
        subject: '填寫這次commit修改了什麼，未來看這段commit msg 自己必須要能理解為原則',
    },
    typePrefix: '[',
    typeSuffix: ']',
    upperCaseSubject: true,
    allowCustomScopes: true,
    skipQuestions: ['body', 'breaking', 'footer', 'confirmCommit'],
};
```

# ChangeLog

```

$ npm install -g conventional-changelog-cli
$ cd my-project
$ conventional-changelog -p angular -i CHANGELOG.md -s
```
