---
title: bookmarmks
date: 2021-10-31 11:40:39
---



# English Learning
* [美國科學人](https://www.scientificamerican.com/)
* [美國科學人 60-Second Science](https://www.scientificamerican.com/podcast/60-second-science/)
* [小笨霖的英文筆記本](https://cc.edutw.net/klee/notebook/index.html)
* [台灣測驗中心-實用基礎文法](http://www.taiwantestcentral.com/Grammar/Title.aspx?ID=314)
* [quizlet](https://quizlet.com/latest)
* [字源](https://www.youdict.net/ciyuan/)
* [engoo](https://engoo.com.tw/app/materials/en)
* [thesaurus](https://www.thesaurus.com/)
  [Grammer](https://www.grammarly.com/) 協助糾正英文文法
* [quizlet](https://quizlet.com 背單字好夥伴
* [VOA](https://learningenglish.voanews.com/) 英文聽力練習
* [同義詞](https://www.powerthesaurus.org/)
* [諷刺英文漫畫](https://www.gocomics.com/)
# 財經
* [市場先生](https://rich01.com/)
# 機械工具 購物
* [春緯精品](https://www.toptul.com.tw/)
* [振宇五金](https://www.ald.com.tw/)
* [鋼咕工具王](https://www.kung-gu.com.tw/)

## 打字練習
* https://www.keybr.com/
*


## 汽車知識

* [末日極限](https://moriteam.com/)
# 電子電路
* [研發互助社區](https://cocdig.com/)
* [電路即時展示](https://www.falstad.com/circuit/circuitjs.html)
* [物理參數動畫](https://ophysics.com/)
* [sparkfun](https://learn.sparkfun.com/)
* [Fritzing forum](https://forum.fritzing.org/)
* [electronicwings](https://www.electronicwings.com/explore)
* [lastminuteengineers](https://lastminuteengineers.com/) 工程師的臨時抱佛腳，快速讓你知道各種感測器的原理，和如何使用的教學文件
* [電子電路圖繪圖網站](https://www.schematics.com/)
* https://interestingengineering.com/
# 軟體學習
* https://www.freecodecamp.org/
* [Rico's cheatsheets](https://devhints.io/) 各種語法紀錄
# 求職
* [Zety Resume Builder](https://zety.com/) Make a Resume Online
* [Senior Software Engineer Resume](https://zety.com/blog/senior-software-engineer-resume-example) software eng example
* [indeed](https://www.indeed.com/) 類似LinkedIn
* [levels](https://www.levels.fyi/) 薪水公開站
* [glassdoor](https://www.glassdoor.com/) - Find The Job That Fits Your Life
* [reddit resumes](https://www.reddit.com/r/resumes/)
# 軟體工程
* [良葛格學習筆記](https://openhome.cc/Gossip/)

## TOOLS

* [base64decode](https://www.base64decode.org/)
* asciiart
    - https://patorjk.com/software/taag
    - https://www.asciiart.eu/
    - https://www.ascii-art-generator.org/
* QR code
    - https://www.qrcode-monkey.com/
    -
* [Xpath cheatsheet](https://devhints.io/xpath)
* [Guru99軟體學習](https://www.guru99.com/)
* [UiPath](https://www.uipath.com/)
* https://www.selenium.dev/
* [Twisted是一個事件驅動的網絡編程框架，它使用程式語言Python編寫](https://twistedmatrix.com/trac/)
* [打字練習](https://www.keybr.com/)

## Network

* [IP查找裝置](https://www.shodan.io/)

## 軟體刷題

* [codewars](https://www.codewars.com/)
* [leetcode](https://leetcode.com/)
* [algoexpert](https://www.algoexpert.io/)
* [hackerearth](https://www.hackerearth.com/practice/)
* [BIG O](https://www.bigocheatsheet.com/)

## 軟體測試

* [軟體品管的專業思維](https://www.qa-knowhow.com/)

## 網站知識

* https://www.w3schools.com/
# 留學
* [考滿分](https://toefl.kmf.com/)
# 咖啡購物
* [臥龍](https://www.wolongcoffee.com/products)
* [虎記商行](https://www.tigercoffee.tw/products)
* [卡卡愛](https://www.kakalovecafe.com.tw/)
# 影片
* [99kubo](http://www.99kubo.tv/)
* [4bigv](http://www.4bigv.com/)
* [劇迷](https://www.gimyvod.cc/)
* [你的動畫](https://youranimes.tw/)
# 名言
* [quotefancy](https://quotefancy.com/)
